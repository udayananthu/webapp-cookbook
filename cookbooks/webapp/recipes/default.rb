#
# Cookbook:: webapp
# Recipe:: default
#
# maintainer:: Aig Cloud Team
# maintainer_email:: aigcloudautomations@aig.com
#
# Copyright:: 2017, Aig Cloud Team, All Rights Reserved.


if (node['platform'] == 'windows')
  chef::Log.warn("This cookbook doesn't support windows platform")

else
  default['webapp']['web_package'] = case node['platform']
    when 'redhat', 'centos'
      httpd
    when 'ubuntu'
      apache2
    end

  web_package = node['webapp']['web_package']

  package 'apache' do
    package_name web_package
    action :install
  end

  service 'apache' do
    service_name web_package
    action [:enabled, :start]
  end

  package 'postgresql' do
    action :install
  end

  service 'postgresql' do
    action [:enable, :start]
  end
  
  postgres_db = data_bag_item('postgres', 'postgres')

  template '/var/opt/postgres/config.yml' do
    action :create_if_missing
    source 'config.yml.erb'
    Variables({
      user: postgres['user'],
      password: postgres['password']
    })
  end
end
