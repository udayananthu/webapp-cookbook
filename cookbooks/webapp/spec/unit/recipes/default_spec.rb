#
# Cookbook:: webapp
# Spec:: default_spec
#
# maintainer:: Aig Cloud Team
# maintainer_email:: aigcloudautomations@aig.com
#
# Copyright:: 2017, Aig Cloud Team, All Rights Reserved.

require 'spec_helper'
require 'chefspec'
describe 'webapp::default' do
  context 'Validate supported installations' do
    platforms = {
      'redhat' => {
        'versions' => %w(6.8 7.2 7.3)
      },
      'centos' => {
        'versions' => %w(6.8 7.2.1511 7.3.1611)
      },
      'ubuntu' => {
        'versions' => %w(14.04 16.04)
      }
    }
    platforms.each do |platform, components|
      components['versions'].each do |version|
        context 'On #{platform} #{version}' do
          context 'When all attributes are default' do
            before do
              Fauxhai.mock(platform: platform, version: version)
            end
            let(:chef_run) do
              ChefSpec::SoloRunner.new(platform: platform, version: version) do |node|
                # Node attributes
              end.converge(described_recipe)

	    it 'install a package with an apache' do
	      except(chef_run).to install_package('apache')
            end

	    it 'start and enable the service apache' do
	      except(chef_run).to start_service('apache')
	      except(chef_run).to enable_service('apache')
	    end

            it 'install a package with postgresql' do
              except(chef_run).to install_package('postgresql')
	    end

            it 'start and enable the service postgresql' do
	      except(chef_run).to start_service('postgresql')
	      except(chef_run).to enable_service('postgresql')
	    end

	    it 'Create a /var/opt/postgres/config.yml template file' do
	      except(chef_run).to create_template('/var/opt/postgres/config.yml')
	    end


            it 'converges successfully' do
              expect { chef_run }.to_not raise_error
            end
          end
        end
      end
    end
  end
end
end
